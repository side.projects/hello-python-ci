FROM alpine:latest

RUN apk add --update python2

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY . /usr/src/app

EXPOSE 8080

CMD ["/usr/bin/python2", "main.py"]